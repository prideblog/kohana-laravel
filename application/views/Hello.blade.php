@extends('layouts.master')

@section('title', 'Page Title')

@section('sidebar')
    @parent

    <p>This is appended to the master sidebar. {{ $test2 }}</p>
@endsection

@section('content')
    <p>This is my body content. {{ $test }}</p>
@endsection