<?php defined('SYSPATH') or die('No direct script access.');

use Illuminate\Database\Capsule\Manager as Capsule;
use Philo\Blade\Blade;

class Controller_Welcome extends Controller {

	public function action_index()
	{
		//$users = Capsule::table('facka')->get();

        $value2 = 'Lorem Ipsum';

        $view = BladeView::factory('Hello');

        $view->bind('test', $value);
        $view->set('test2', $value2);
        $value = 'Timeo Danaos et dona ferentes';

        $this->response->body($view->render());
	}

} // End Welcome